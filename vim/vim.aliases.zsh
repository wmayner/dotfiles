# use servers with vim
alias vim='/Applications/MacVim.app/Contents/MacOS/Vim --servername VIM'
# use MacVim as gvim
alias gvim="open -a MacVim"
# edit local .vimrc
alias vrc="vim ~/.vimrc.local"
# edit .vimrc
alias vrcreal="vim ~/.vimrc"
# add/remove vim bundles with vundle
alias vbun="vim ~/.vimrc.bundles.local"
